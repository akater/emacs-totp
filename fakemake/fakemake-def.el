;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'totp
  authors "Dima Akater"
  first-publication-year-as-string "2023"
  org-files-in-order '("totp" "totp-import")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda () (require 'akater-misc-buffers-macs)))
